""""""
directive @cacheControl(maxAge: Int, scope: CacheControlScope) on FIELD_DEFINITION | OBJECT | INTERFACE

"""
Direct the client to resolve this field locally, either from the cache or local resolvers.
"""
directive @client(
  """
  When true, the client will never use the cache for this value. See
  https://www.apollographql.com/docs/react/essentials/local-state/#forcing-resolvers-with-clientalways-true
  """
  always: Boolean
) on FIELD | FRAGMENT_DEFINITION | INLINE_FRAGMENT

"""
Export this locally resolved field as a variable to be used in the remainder of this query. See
https://www.apollographql.com/docs/react/essentials/local-state/#using-client-fields-as-variables
"""
directive @export(
  """The variable name to export this field as."""
  as: String!
) on FIELD

"""
Specify a custom store key for this result. See
https://www.apollographql.com/docs/react/advanced/caching/#the-connection-directive
"""
directive @connection(
  """Specify the store key."""
  key: String!

  """
  An array of query argument names to include in the generated custom store key.
  """
  filter: [String!]
) on FIELD

""""""
type Query {
  """"""
  article(id: ID!): Articles

  """"""
  articles(sort: String, limit: Int, start: Int, where: JSON): [Articles]

  """"""
  articlesConnection(sort: String, limit: Int, start: Int, where: JSON): ArticlesConnection

  """"""
  category(id: ID!): Category

  """"""
  categories(sort: String, limit: Int, start: Int, where: JSON): [Category]

  """"""
  categoriesConnection(sort: String, limit: Int, start: Int, where: JSON): CategoryConnection

  """"""
  files(sort: String, limit: Int, start: Int, where: JSON): [UploadFile]

  """"""
  filesConnection(sort: String, limit: Int, start: Int, where: JSON): UploadFileConnection

  """"""
  role(id: ID!): UsersPermissionsRole

  """
  Retrieve all the existing roles. You can't apply filters on this query.
  """
  roles(sort: String, limit: Int, start: Int, where: JSON): [UsersPermissionsRole]

  """"""
  rolesConnection(sort: String, limit: Int, start: Int, where: JSON): UsersPermissionsRoleConnection

  """"""
  user(id: ID!): UsersPermissionsUser

  """"""
  users(sort: String, limit: Int, start: Int, where: JSON): [UsersPermissionsUser]

  """"""
  usersConnection(sort: String, limit: Int, start: Int, where: JSON): UsersPermissionsUserConnection

  """"""
  me: UsersPermissionsMe
}

""""""
type Articles {
  """"""
  id: ID!

  """"""
  created_at: DateTime!

  """"""
  updated_at: DateTime!

  """"""
  title: String

  """"""
  description: String

  """"""
  published_at: Date

  """"""
  category: Category

  """"""
  created_by: AdminUser

  """"""
  updated_by: AdminUser
}

"""
A date-time string at UTC, such as 2007-12-03T10:15:30Z, compliant with the `date-time` format outlined in section 5.6 of the RFC 3339 profile of the ISO 8601 standard for representation of dates and times using the Gregorian calendar.
"""
scalar DateTime

"""
A date string, such as 2007-12-03, compliant with the `full-date` format outlined in section 5.6 of the RFC 3339 profile of the ISO 8601 standard for representation of dates and times using the Gregorian calendar.
"""
scalar Date

""""""
type Category {
  """"""
  id: ID!

  """"""
  created_at: DateTime!

  """"""
  updated_at: DateTime!

  """"""
  name: String

  """"""
  created_by: AdminUser

  """"""
  updated_by: AdminUser

  """"""
  articles(sort: String, limit: Int, start: Int, where: JSON): [Articles]
}

""""""
type AdminUser {
  """"""
  id: ID!

  """"""
  username: String
}

"""
The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf).
"""
scalar JSON

""""""
type ArticlesConnection {
  """"""
  values: [Articles]

  """"""
  groupBy: ArticlesGroupBy

  """"""
  aggregate: ArticlesAggregator
}

""""""
type ArticlesGroupBy {
  """"""
  id: [ArticlesConnectionId]

  """"""
  created_at: [ArticlesConnectionCreated_at]

  """"""
  updated_at: [ArticlesConnectionUpdated_at]

  """"""
  title: [ArticlesConnectionTitle]

  """"""
  description: [ArticlesConnectionDescription]

  """"""
  published_at: [ArticlesConnectionPublished_at]

  """"""
  category: [ArticlesConnectionCategory]

  """"""
  created_by: [ArticlesConnectionCreated_by]

  """"""
  updated_by: [ArticlesConnectionUpdated_by]
}

""""""
type ArticlesConnectionId {
  """"""
  key: ID

  """"""
  connection: ArticlesConnection
}

""""""
type ArticlesConnectionCreated_at {
  """"""
  key: DateTime

  """"""
  connection: ArticlesConnection
}

""""""
type ArticlesConnectionUpdated_at {
  """"""
  key: DateTime

  """"""
  connection: ArticlesConnection
}

""""""
type ArticlesConnectionTitle {
  """"""
  key: String

  """"""
  connection: ArticlesConnection
}

""""""
type ArticlesConnectionDescription {
  """"""
  key: String

  """"""
  connection: ArticlesConnection
}

""""""
type ArticlesConnectionPublished_at {
  """"""
  key: ID

  """"""
  connection: ArticlesConnection
}

""""""
type ArticlesConnectionCategory {
  """"""
  key: ID

  """"""
  connection: ArticlesConnection
}

""""""
type ArticlesConnectionCreated_by {
  """"""
  key: ID

  """"""
  connection: ArticlesConnection
}

""""""
type ArticlesConnectionUpdated_by {
  """"""
  key: ID

  """"""
  connection: ArticlesConnection
}

""""""
type ArticlesAggregator {
  """"""
  count: Int

  """"""
  totalCount: Int
}

""""""
type CategoryConnection {
  """"""
  values: [Category]

  """"""
  groupBy: CategoryGroupBy

  """"""
  aggregate: CategoryAggregator
}

""""""
type CategoryGroupBy {
  """"""
  id: [CategoryConnectionId]

  """"""
  created_at: [CategoryConnectionCreated_at]

  """"""
  updated_at: [CategoryConnectionUpdated_at]

  """"""
  name: [CategoryConnectionName]

  """"""
  created_by: [CategoryConnectionCreated_by]

  """"""
  updated_by: [CategoryConnectionUpdated_by]
}

""""""
type CategoryConnectionId {
  """"""
  key: ID

  """"""
  connection: CategoryConnection
}

""""""
type CategoryConnectionCreated_at {
  """"""
  key: DateTime

  """"""
  connection: CategoryConnection
}

""""""
type CategoryConnectionUpdated_at {
  """"""
  key: DateTime

  """"""
  connection: CategoryConnection
}

""""""
type CategoryConnectionName {
  """"""
  key: String

  """"""
  connection: CategoryConnection
}

""""""
type CategoryConnectionCreated_by {
  """"""
  key: ID

  """"""
  connection: CategoryConnection
}

""""""
type CategoryConnectionUpdated_by {
  """"""
  key: ID

  """"""
  connection: CategoryConnection
}

""""""
type CategoryAggregator {
  """"""
  count: Int

  """"""
  totalCount: Int
}

""""""
type UploadFile {
  """"""
  id: ID!

  """"""
  created_at: DateTime!

  """"""
  updated_at: DateTime!

  """"""
  name: String!

  """"""
  alternativeText: String

  """"""
  caption: String

  """"""
  width: Int

  """"""
  height: Int

  """"""
  formats: JSON

  """"""
  hash: String!

  """"""
  ext: String

  """"""
  mime: String!

  """"""
  size: Float!

  """"""
  url: String!

  """"""
  previewUrl: String

  """"""
  provider: String!

  """"""
  provider_metadata: JSON

  """"""
  created_by: AdminUser

  """"""
  updated_by: AdminUser

  """"""
  related(sort: String, limit: Int, start: Int, where: JSON): [Morph]
}

""""""
union Morph = UsersPermissionsMe | UsersPermissionsMeRole | UsersPermissionsLoginPayload | UserPermissionsPasswordPayload | Articles | ArticlesConnection | ArticlesAggregator | ArticlesGroupBy | ArticlesConnectionId | ArticlesConnectionCreated_at | ArticlesConnectionUpdated_at | ArticlesConnectionTitle | ArticlesConnectionDescription | ArticlesConnectionPublished_at | ArticlesConnectionCategory | ArticlesConnectionCreated_by | ArticlesConnectionUpdated_by | createArticlePayload | updateArticlePayload | deleteArticlePayload | Category | CategoryConnection | CategoryAggregator | CategoryGroupBy | CategoryConnectionId | CategoryConnectionCreated_at | CategoryConnectionUpdated_at | CategoryConnectionName | CategoryConnectionCreated_by | CategoryConnectionUpdated_by | createCategoryPayload | updateCategoryPayload | deleteCategoryPayload | UploadFile | UploadFileConnection | UploadFileAggregator | UploadFileAggregatorSum | UploadFileAggregatorAvg | UploadFileAggregatorMin | UploadFileAggregatorMax | UploadFileGroupBy | UploadFileConnectionId | UploadFileConnectionCreated_at | UploadFileConnectionUpdated_at | UploadFileConnectionName | UploadFileConnectionAlternativeText | UploadFileConnectionCaption | UploadFileConnectionWidth | UploadFileConnectionHeight | UploadFileConnectionFormats | UploadFileConnectionHash | UploadFileConnectionExt | UploadFileConnectionMime | UploadFileConnectionSize | UploadFileConnectionUrl | UploadFileConnectionPreviewUrl | UploadFileConnectionProvider | UploadFileConnectionProvider_metadata | UploadFileConnectionCreated_by | UploadFileConnectionUpdated_by | UsersPermissionsPermission | UsersPermissionsRole | UsersPermissionsRoleConnection | UsersPermissionsRoleAggregator | UsersPermissionsRoleGroupBy | UsersPermissionsRoleConnectionId | UsersPermissionsRoleConnectionName | UsersPermissionsRoleConnectionDescription | UsersPermissionsRoleConnectionType | UsersPermissionsRoleConnectionCreated_by | UsersPermissionsRoleConnectionUpdated_by | createRolePayload | updateRolePayload | deleteRolePayload | UsersPermissionsUser | UsersPermissionsUserConnection | UsersPermissionsUserAggregator | UsersPermissionsUserGroupBy | UsersPermissionsUserConnectionId | UsersPermissionsUserConnectionCreated_at | UsersPermissionsUserConnectionUpdated_at | UsersPermissionsUserConnectionUsername | UsersPermissionsUserConnectionEmail | UsersPermissionsUserConnectionProvider | UsersPermissionsUserConnectionConfirmed | UsersPermissionsUserConnectionBlocked | UsersPermissionsUserConnectionRole | UsersPermissionsUserConnectionCreated_by | UsersPermissionsUserConnectionUpdated_by | createUserPayload | updateUserPayload | deleteUserPayload

""""""
type UsersPermissionsMe {
  """"""
  id: ID!

  """"""
  username: String!

  """"""
  email: String!

  """"""
  confirmed: Boolean

  """"""
  blocked: Boolean

  """"""
  role: UsersPermissionsMeRole
}

""""""
type UsersPermissionsMeRole {
  """"""
  id: ID!

  """"""
  name: String!

  """"""
  description: String

  """"""
  type: String
}

""""""
type UsersPermissionsLoginPayload {
  """"""
  jwt: String

  """"""
  user: UsersPermissionsMe!
}

""""""
type UserPermissionsPasswordPayload {
  """"""
  ok: Boolean!
}

""""""
type createArticlePayload {
  """"""
  article: Articles
}

""""""
type updateArticlePayload {
  """"""
  article: Articles
}

""""""
type deleteArticlePayload {
  """"""
  article: Articles
}

""""""
type createCategoryPayload {
  """"""
  category: Category
}

""""""
type updateCategoryPayload {
  """"""
  category: Category
}

""""""
type deleteCategoryPayload {
  """"""
  category: Category
}

""""""
type UploadFileConnection {
  """"""
  values: [UploadFile]

  """"""
  groupBy: UploadFileGroupBy

  """"""
  aggregate: UploadFileAggregator
}

""""""
type UploadFileGroupBy {
  """"""
  id: [UploadFileConnectionId]

  """"""
  created_at: [UploadFileConnectionCreated_at]

  """"""
  updated_at: [UploadFileConnectionUpdated_at]

  """"""
  name: [UploadFileConnectionName]

  """"""
  alternativeText: [UploadFileConnectionAlternativeText]

  """"""
  caption: [UploadFileConnectionCaption]

  """"""
  width: [UploadFileConnectionWidth]

  """"""
  height: [UploadFileConnectionHeight]

  """"""
  formats: [UploadFileConnectionFormats]

  """"""
  hash: [UploadFileConnectionHash]

  """"""
  ext: [UploadFileConnectionExt]

  """"""
  mime: [UploadFileConnectionMime]

  """"""
  size: [UploadFileConnectionSize]

  """"""
  url: [UploadFileConnectionUrl]

  """"""
  previewUrl: [UploadFileConnectionPreviewUrl]

  """"""
  provider: [UploadFileConnectionProvider]

  """"""
  provider_metadata: [UploadFileConnectionProvider_metadata]

  """"""
  created_by: [UploadFileConnectionCreated_by]

  """"""
  updated_by: [UploadFileConnectionUpdated_by]
}

""""""
type UploadFileConnectionId {
  """"""
  key: ID

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionCreated_at {
  """"""
  key: DateTime

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionUpdated_at {
  """"""
  key: DateTime

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionName {
  """"""
  key: String

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionAlternativeText {
  """"""
  key: String

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionCaption {
  """"""
  key: String

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionWidth {
  """"""
  key: Int

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionHeight {
  """"""
  key: Int

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionFormats {
  """"""
  key: JSON

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionHash {
  """"""
  key: String

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionExt {
  """"""
  key: String

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionMime {
  """"""
  key: String

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionSize {
  """"""
  key: Float

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionUrl {
  """"""
  key: String

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionPreviewUrl {
  """"""
  key: String

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionProvider {
  """"""
  key: String

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionProvider_metadata {
  """"""
  key: JSON

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionCreated_by {
  """"""
  key: ID

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileConnectionUpdated_by {
  """"""
  key: ID

  """"""
  connection: UploadFileConnection
}

""""""
type UploadFileAggregator {
  """"""
  count: Int

  """"""
  totalCount: Int

  """"""
  sum: UploadFileAggregatorSum

  """"""
  avg: UploadFileAggregatorAvg

  """"""
  min: UploadFileAggregatorMin

  """"""
  max: UploadFileAggregatorMax
}

""""""
type UploadFileAggregatorSum {
  """"""
  width: Float

  """"""
  height: Float

  """"""
  size: Float
}

""""""
type UploadFileAggregatorAvg {
  """"""
  width: Float

  """"""
  height: Float

  """"""
  size: Float
}

""""""
type UploadFileAggregatorMin {
  """"""
  width: Float

  """"""
  height: Float

  """"""
  size: Float
}

""""""
type UploadFileAggregatorMax {
  """"""
  width: Float

  """"""
  height: Float

  """"""
  size: Float
}

""""""
type UsersPermissionsPermission {
  """"""
  id: ID!

  """"""
  type: String!

  """"""
  controller: String!

  """"""
  action: String!

  """"""
  enabled: Boolean!

  """"""
  policy: String

  """"""
  role: UsersPermissionsRole

  """"""
  created_by: AdminUser

  """"""
  updated_by: AdminUser
}

""""""
type UsersPermissionsRole {
  """"""
  id: ID!

  """"""
  name: String!

  """"""
  description: String

  """"""
  type: String

  """"""
  created_by: AdminUser

  """"""
  updated_by: AdminUser

  """"""
  permissions(sort: String, limit: Int, start: Int, where: JSON): [UsersPermissionsPermission]

  """"""
  users(sort: String, limit: Int, start: Int, where: JSON): [UsersPermissionsUser]
}

""""""
type UsersPermissionsUser {
  """"""
  id: ID!

  """"""
  created_at: DateTime!

  """"""
  updated_at: DateTime!

  """"""
  username: String!

  """"""
  email: String!

  """"""
  provider: String

  """"""
  confirmed: Boolean

  """"""
  blocked: Boolean

  """"""
  role: UsersPermissionsRole

  """"""
  created_by: AdminUser

  """"""
  updated_by: AdminUser
}

""""""
type UsersPermissionsRoleConnection {
  """"""
  values: [UsersPermissionsRole]

  """"""
  groupBy: UsersPermissionsRoleGroupBy

  """"""
  aggregate: UsersPermissionsRoleAggregator
}

""""""
type UsersPermissionsRoleGroupBy {
  """"""
  id: [UsersPermissionsRoleConnectionId]

  """"""
  name: [UsersPermissionsRoleConnectionName]

  """"""
  description: [UsersPermissionsRoleConnectionDescription]

  """"""
  type: [UsersPermissionsRoleConnectionType]

  """"""
  created_by: [UsersPermissionsRoleConnectionCreated_by]

  """"""
  updated_by: [UsersPermissionsRoleConnectionUpdated_by]
}

""""""
type UsersPermissionsRoleConnectionId {
  """"""
  key: ID

  """"""
  connection: UsersPermissionsRoleConnection
}

""""""
type UsersPermissionsRoleConnectionName {
  """"""
  key: String

  """"""
  connection: UsersPermissionsRoleConnection
}

""""""
type UsersPermissionsRoleConnectionDescription {
  """"""
  key: String

  """"""
  connection: UsersPermissionsRoleConnection
}

""""""
type UsersPermissionsRoleConnectionType {
  """"""
  key: String

  """"""
  connection: UsersPermissionsRoleConnection
}

""""""
type UsersPermissionsRoleConnectionCreated_by {
  """"""
  key: ID

  """"""
  connection: UsersPermissionsRoleConnection
}

""""""
type UsersPermissionsRoleConnectionUpdated_by {
  """"""
  key: ID

  """"""
  connection: UsersPermissionsRoleConnection
}

""""""
type UsersPermissionsRoleAggregator {
  """"""
  count: Int

  """"""
  totalCount: Int
}

""""""
type createRolePayload {
  """"""
  role: UsersPermissionsRole
}

""""""
type updateRolePayload {
  """"""
  role: UsersPermissionsRole
}

""""""
type deleteRolePayload {
  """"""
  role: UsersPermissionsRole
}

""""""
type UsersPermissionsUserConnection {
  """"""
  values: [UsersPermissionsUser]

  """"""
  groupBy: UsersPermissionsUserGroupBy

  """"""
  aggregate: UsersPermissionsUserAggregator
}

""""""
type UsersPermissionsUserGroupBy {
  """"""
  id: [UsersPermissionsUserConnectionId]

  """"""
  created_at: [UsersPermissionsUserConnectionCreated_at]

  """"""
  updated_at: [UsersPermissionsUserConnectionUpdated_at]

  """"""
  username: [UsersPermissionsUserConnectionUsername]

  """"""
  email: [UsersPermissionsUserConnectionEmail]

  """"""
  provider: [UsersPermissionsUserConnectionProvider]

  """"""
  confirmed: [UsersPermissionsUserConnectionConfirmed]

  """"""
  blocked: [UsersPermissionsUserConnectionBlocked]

  """"""
  role: [UsersPermissionsUserConnectionRole]

  """"""
  created_by: [UsersPermissionsUserConnectionCreated_by]

  """"""
  updated_by: [UsersPermissionsUserConnectionUpdated_by]
}

""""""
type UsersPermissionsUserConnectionId {
  """"""
  key: ID

  """"""
  connection: UsersPermissionsUserConnection
}

""""""
type UsersPermissionsUserConnectionCreated_at {
  """"""
  key: DateTime

  """"""
  connection: UsersPermissionsUserConnection
}

""""""
type UsersPermissionsUserConnectionUpdated_at {
  """"""
  key: DateTime

  """"""
  connection: UsersPermissionsUserConnection
}

""""""
type UsersPermissionsUserConnectionUsername {
  """"""
  key: String

  """"""
  connection: UsersPermissionsUserConnection
}

""""""
type UsersPermissionsUserConnectionEmail {
  """"""
  key: String

  """"""
  connection: UsersPermissionsUserConnection
}

""""""
type UsersPermissionsUserConnectionProvider {
  """"""
  key: String

  """"""
  connection: UsersPermissionsUserConnection
}

""""""
type UsersPermissionsUserConnectionConfirmed {
  """"""
  key: Boolean

  """"""
  connection: UsersPermissionsUserConnection
}

""""""
type UsersPermissionsUserConnectionBlocked {
  """"""
  key: Boolean

  """"""
  connection: UsersPermissionsUserConnection
}

""""""
type UsersPermissionsUserConnectionRole {
  """"""
  key: ID

  """"""
  connection: UsersPermissionsUserConnection
}

""""""
type UsersPermissionsUserConnectionCreated_by {
  """"""
  key: ID

  """"""
  connection: UsersPermissionsUserConnection
}

""""""
type UsersPermissionsUserConnectionUpdated_by {
  """"""
  key: ID

  """"""
  connection: UsersPermissionsUserConnection
}

""""""
type UsersPermissionsUserAggregator {
  """"""
  count: Int

  """"""
  totalCount: Int
}

""""""
type createUserPayload {
  """"""
  user: UsersPermissionsUser
}

""""""
type updateUserPayload {
  """"""
  user: UsersPermissionsUser
}

""""""
type deleteUserPayload {
  """"""
  user: UsersPermissionsUser
}

""""""
type Mutation {
  """"""
  createArticle(input: createArticleInput): createArticlePayload

  """"""
  updateArticle(input: updateArticleInput): updateArticlePayload

  """"""
  deleteArticle(input: deleteArticleInput): deleteArticlePayload

  """"""
  createCategory(input: createCategoryInput): createCategoryPayload

  """"""
  updateCategory(input: updateCategoryInput): updateCategoryPayload

  """"""
  deleteCategory(input: deleteCategoryInput): deleteCategoryPayload

  """Create a new role"""
  createRole(input: createRoleInput): createRolePayload

  """Update an existing role"""
  updateRole(input: updateRoleInput): updateRolePayload

  """Delete an existing role"""
  deleteRole(input: deleteRoleInput): deleteRolePayload

  """Create a new user"""
  createUser(input: createUserInput): createUserPayload

  """Update an existing user"""
  updateUser(input: updateUserInput): updateUserPayload

  """Delete an existing user"""
  deleteUser(input: deleteUserInput): deleteUserPayload

  """"""
  upload(refId: ID, ref: String, field: String, source: String, file: Upload!): UploadFile!

  """"""
  multipleUpload(refId: ID, ref: String, field: String, source: String, files: [Upload]!): [UploadFile]!

  """"""
  login(input: UsersPermissionsLoginInput!): UsersPermissionsLoginPayload!

  """"""
  register(input: UsersPermissionsRegisterInput!): UsersPermissionsLoginPayload!

  """"""
  forgotPassword(email: String!): UserPermissionsPasswordPayload

  """"""
  resetPassword(password: String!, passwordConfirmation: String!, code: String!): UsersPermissionsLoginPayload

  """"""
  emailConfirmation(confirmation: String!): UsersPermissionsLoginPayload
}

""""""
input createArticleInput {
  """"""
  data: ArticleInput
}

""""""
input ArticleInput {
  """"""
  title: String

  """"""
  description: String

  """"""
  published_at: Date

  """"""
  category: ID

  """"""
  created_by: ID

  """"""
  updated_by: ID
}

""""""
input updateArticleInput {
  """"""
  where: InputID

  """"""
  data: editArticleInput
}

""""""
input InputID {
  """"""
  id: ID!
}

""""""
input editArticleInput {
  """"""
  title: String

  """"""
  description: String

  """"""
  published_at: Date

  """"""
  category: ID

  """"""
  created_by: ID

  """"""
  updated_by: ID
}

""""""
input deleteArticleInput {
  """"""
  where: InputID
}

""""""
input createCategoryInput {
  """"""
  data: CategoryInput
}

""""""
input CategoryInput {
  """"""
  name: String

  """"""
  articles: [ID]

  """"""
  created_by: ID

  """"""
  updated_by: ID
}

""""""
input updateCategoryInput {
  """"""
  where: InputID

  """"""
  data: editCategoryInput
}

""""""
input editCategoryInput {
  """"""
  name: String

  """"""
  articles: [ID]

  """"""
  created_by: ID

  """"""
  updated_by: ID
}

""""""
input deleteCategoryInput {
  """"""
  where: InputID
}

""""""
input createRoleInput {
  """"""
  data: RoleInput
}

""""""
input RoleInput {
  """"""
  name: String!

  """"""
  description: String

  """"""
  type: String

  """"""
  permissions: [ID]

  """"""
  users: [ID]

  """"""
  created_by: ID

  """"""
  updated_by: ID
}

""""""
input updateRoleInput {
  """"""
  where: InputID

  """"""
  data: editRoleInput
}

""""""
input editRoleInput {
  """"""
  name: String

  """"""
  description: String

  """"""
  type: String

  """"""
  permissions: [ID]

  """"""
  users: [ID]

  """"""
  created_by: ID

  """"""
  updated_by: ID
}

""""""
input deleteRoleInput {
  """"""
  where: InputID
}

""""""
input createUserInput {
  """"""
  data: UserInput
}

""""""
input UserInput {
  """"""
  username: String!

  """"""
  email: String!

  """"""
  provider: String

  """"""
  password: String

  """"""
  resetPasswordToken: String

  """"""
  confirmed: Boolean

  """"""
  blocked: Boolean

  """"""
  role: ID

  """"""
  created_by: ID

  """"""
  updated_by: ID
}

""""""
input updateUserInput {
  """"""
  where: InputID

  """"""
  data: editUserInput
}

""""""
input editUserInput {
  """"""
  username: String

  """"""
  email: String

  """"""
  provider: String

  """"""
  password: String

  """"""
  resetPasswordToken: String

  """"""
  confirmed: Boolean

  """"""
  blocked: Boolean

  """"""
  role: ID

  """"""
  created_by: ID

  """"""
  updated_by: ID
}

""""""
input deleteUserInput {
  """"""
  where: InputID
}

"""The `Upload` scalar type represents a file upload."""
scalar Upload

""""""
input UsersPermissionsLoginInput {
  """"""
  identifier: String!

  """"""
  password: String!

  """"""
  provider: String = "local"
}

""""""
input UsersPermissionsRegisterInput {
  """"""
  username: String!

  """"""
  email: String!

  """"""
  password: String!
}

""""""
input FileInput {
  """"""
  name: String!

  """"""
  alternativeText: String

  """"""
  caption: String

  """"""
  width: Int

  """"""
  height: Int

  """"""
  formats: JSON

  """"""
  hash: String!

  """"""
  ext: String

  """"""
  mime: String!

  """"""
  size: Float!

  """"""
  url: String!

  """"""
  previewUrl: String

  """"""
  provider: String!

  """"""
  provider_metadata: JSON

  """"""
  related: [ID]

  """"""
  created_by: ID

  """"""
  updated_by: ID
}

""""""
input editFileInput {
  """"""
  name: String

  """"""
  alternativeText: String

  """"""
  caption: String

  """"""
  width: Int

  """"""
  height: Int

  """"""
  formats: JSON

  """"""
  hash: String

  """"""
  ext: String

  """"""
  mime: String

  """"""
  size: Float

  """"""
  url: String

  """"""
  previewUrl: String

  """"""
  provider: String

  """"""
  provider_metadata: JSON

  """"""
  related: [ID]

  """"""
  created_by: ID

  """"""
  updated_by: ID
}

"""A time string with format: HH:mm:ss.SSS"""
scalar Time

"""The `Long` scalar type represents 52-bit integers"""
scalar Long

""""""
enum CacheControlScope {
  """"""
  PUBLIC

  """"""
  PRIVATE
}

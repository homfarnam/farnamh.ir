/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Categories
// ====================================================

export interface Categories_categories {
  __typename: "Category"
  id: string
  name: string | null
}

export interface Categories {
  categories: (Categories_categories | null)[] | null
}
